# django
from django.contrib import admin

# models
from students.models import Student


# Register your models here.
admin.site.register(Student)