# django
from django.db import models
from django.conf import settings

# constants
SEMESTERS = (
    (1, 'First'),
    (2, 'Second'),
    (3, 'Third'),
    (4, 'Fourth'),
    (5, 'Fifth'),
    (6, 'Sixth'),
    (7, 'Seventh'),
    (8, 'Eighth'),
    (9, 'Ninth'),
    (10, 'Tenth'),
    (11, 'Eleventh'),
    (12, 'Twelfth'),
)	


# Create your models here.
class Student(models.Model):
    user = models.OneToOneField(
        to=settings.AUTH_USER_MODEL, 
		on_delete=models.CASCADE,
    )

    period = models.SmallIntegerField(
        choices=SEMESTERS,
		default=1
    )

    is_active = models.BooleanField(
        default=True
    )
