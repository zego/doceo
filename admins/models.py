# django
from django.db import models
from django.conf import settings


# Create your models here.
class Administrator(models.Model):
    user = models.OneToOneField(
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    number = models.CharField(
        max_length=10,
        unique=True
    )

    office_number = models.CharField(
        max_length=10
    )

    is_active = models.BooleanField(
        default=True
    )
