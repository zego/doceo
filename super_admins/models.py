# django
from django.db import models
from django.conf import settings


# Create your models here.
class SuperAdministrator(models.Model):
    user = models.OneToOneField(
        to=settings.AUTH_USER_MODEL, 
		on_delete=models.CASCADE,
    )

    is_active = models.BooleanField(
        default=True
    )
