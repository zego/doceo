from django.apps import AppConfig


class SuperAdminsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'super_admins'
