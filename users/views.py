# Django
from django.contrib.auth import authenticate, logout
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie

# Django rest framework
from rest_framework import status, exceptions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny

# Models
from users.models import User

# Serializers
from users.serializers import UserSerializer, StudentSignUpSerializer, TeacherSignUpSerializer

# Utilities
from users.utils import generate_token

# Exceptions
from users.exceptions import UserRoleException


# Create your views here.
class UserSignUpAPIView(APIView):
    """
        User Sign Up APIView
        This endpoint handles the signup process.
        This endpoint delegates almos the whole process to the serializer
        and only returns the user data and a status if all the process is
        successfully done.
    """
    permission_classes = [AllowAny,]
    serializers = {
        1: StudentSignUpSerializer,
        2: TeacherSignUpSerializer
    }

    def post(self, request: dict, *args: list, **kwargs: dict) -> Response:
        """"""
        user_role = int(request.data.get('user_role', 0))
        if user_role and user_role in range(1, 5):
            serializer = UserSignUpAPIView.serializers.get(user_role)(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(data={"message": "User created succesfully."}, status=status.HTTP_201_CREATED)

        raise UserRoleException('The given user role is not a valid user role')


class UserLoginAPIView(APIView):
    """
        User Log In APIView.
        This endpoint handles the login process.
        This endpoint returns an access token and the user data.
    """
    permission_classes: list = [AllowAny,]

    @method_decorator(ensure_csrf_cookie)
    def post(self, request: dict, *args: list, **kwargs: dict) -> Response:
        """"""
        email = request.data.get('email', None)
        password = request.data.get('password', None)

        if not email or not password:
            raise exceptions.AuthenticationFailed('email and password required')

        user = authenticate(email=email, password=password)

        if user is None:
            raise exceptions.AuthenticationFailed('User not found')

        if not user.check_password(password):
            raise exceptions.AuthenticationFailed('Wrong password')

        access_token = generate_token(user, 'access')
        refresh_token = generate_token(user, 'refresh')

        response = Response()
        response.set_cookie(key='key', value=access_token['key'], httponly=True)
        response.data = {
            'access-token': access_token['token'],
            'refresh-token': refresh_token['token'],
        }
        response.status = status.HTTP_201_CREATED

        return response


class UserLogoutAPIView(APIView):
    permission_classes = [IsAuthenticated,]

    def post(self, request: dict, *args: list, **kwargs: dict) -> Response:
        response = Response()
        response.delete_cookie('csrftoken')
        response.delete_cookie('key')
        generate_token(request.user, 'logout')
        response.data = {'message': 'User logged out succesfully'}
        response.status = status.HTTP_200_OK
        return response


class UsersAPIView(APIView):
    """
        Users Up APIView
        This endpoint is to test the custom JWT implemnetation.
    """
    permission_classes = [IsAuthenticated,]

    def get(self, request: dict, *args: list, **kwargs: dict) -> Response:
        """"""
        user = request.user
        serialized_user = UserSerializer(user).data
        return Response(data=serialized_user, status=status.HTTP_200_OK)
