# Django
from django.urls import path

# Views
import users.views as views


app_name: str ='users'
apiview_urls: list = [
    path(
        route='signup/',
        view=views.UserSignUpAPIView.as_view(),
        name='signup'
    ),
    path(
        route='login/',
        view=views.UserLoginAPIView.as_view(),
        name='login'
    ),
    path(
        route='logout/',
        view=views.UserLogoutAPIView.as_view(),
        name='logout'
    ),
    path(
        route='user/',
        view=views.UsersAPIView.as_view(),
        name='user'
    ),
]

urlpatterns: list = apiview_urls
