# Django
from django.contrib import admin

# Models
from users.models import User


# Register your models here.
admin.site.register(User)