# Django
from django.conf import settings
from django.utils import timezone

# Utilities
import os
from datetime import timedelta
from jwcrypto import jwt, jwk

# constants
TOKEN_EPIRATION = {
    'access': timezone.now() + timedelta(hours=5),
    'refresh': timezone.now() + timedelta(days=1),
    'logout': timezone.now() - timedelta(minutes=5),
}


def get_upload_path(instance, filename):
    """
        get_upload_path function.
        This function is used to get the upload path for image upload.
        By receiving the instance of the object and the filename, the function
        ensurances that the images ara uploaded to the correct dir.
    """
    return os.path.join(
        f'{instance.pk}',
        f'{filename}'
    )

def generate_token(user: 'User', type: str) -> dict:
    key = jwk.JWK(generate='oct', size=256)
    
    exp_date = TOKEN_EPIRATION[type]

    token = jwt.JWT(
        header={
            'alg': 'HS256',
            'typ': 'JWT'
        },
        claims={
            'email': user.email,
            'dossier': user.dossier,
            'id': user.id,
            'exp': int(exp_date.timestamp()),
        }
    )
    token.make_signed_token(key)

    return {
        'token': token.serialize(),
        'key': key.export()
    }
