# Django
from django.conf import settings
from django.middleware.csrf import CsrfViewMiddleware

#  Django rest framework
from rest_framework.authentication import BaseAuthentication
from rest_framework import exceptions

# Models
from users.models import User

# Utilities
from jwcrypto import jwt, jwk
import json


class CSRFCheck(CsrfViewMiddleware):

    def _reject(self, request, reason):
        # Return the failure reason instead of an HttpResponse
        return reason


class SafeJWTAuthentication(BaseAuthentication):
    """
        SafeJWTAuthentication class.
        This class is a custom authentication class for DRF and JWT.
        You can find the original code here: https://github.com/encode/django-rest-framework/blob/master/rest_framework/authentication.py
    """

    def authenticate(self, request):
        authorization_header = request.headers.get('Authorization')

        if not authorization_header:
            return None
        try:
            # header = 'Token xxxxxxxxxxxxxxxxxxxxxxxx'
            access_token = authorization_header.split(' ')[1]
            k = json.loads(request.COOKIES.get('key'))
            key = jwk.JWK(**k)
            payload = jwt.JWT(key=key, jwt=access_token)
        except jwt.JWTExpired:
            raise exceptions.AuthenticationFailed('access_token expired')
        except IndexError:
            raise exceptions.AuthenticationFailed('Token prefix missing')
        except:
            raise exceptions.AuthenticationFailed('Invalid Token')

        payload = json.loads(payload.claims)
        user = User.objects.filter(id=payload['id']).first()

        if user is None:
            raise exceptions.AuthenticationFailed('User not found')

        if not user.is_active:
            raise exceptions.AuthenticationFailed('user is inactive')

        self.enforce_csrf(request)

        return (user, None)

    def enforce_csrf(self, request):
        """
            Enforce CSRF validation
        """
        check = CSRFCheck()
        # populates request.META['CSRF_COOKIE'], which is used in process_view()
        check.process_request(request)
        reason = check.process_view(request, None, (), {})
        if reason:
            # CSRF failed, bail with explicit error message
            raise exceptions.PermissionDenied('CSRF Failed: %s' % reason)