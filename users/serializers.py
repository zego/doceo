# Django
from django.contrib.auth import password_validation

# Django rest framwork
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

# Models
from users.models import User

# factories
from users.factories import UserRoleFactory


class UserSerializer(serializers.ModelSerializer):
    """
        User Serializer.
        This serializer has the main function of serialize the User
        data given from the model.
    """
    class Meta:
        """
            Class Meta
            This class contains metada that tell the class how to
            behave. For example the fields that are going to be
            returned is in the fields variable.
        """
        model = User
        fields = [
            'id', 'email', 'dossier',
            'first_name', 'last_name', 'is_active',
            'user_role', 'picture', 'about'
        ]


class BaseUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'email', 'dossier', 'user_role',
            'first_name', 'last_name', 'password',
        )


class StudentSignUpSerializer(BaseUserSerializer):
    """
        User Sign Up Serializer.
        This serializer handles the process of validate data and create
        the user.
    """
    password_confirmation = serializers.CharField(
        min_length=8
    )

    period = serializers.IntegerField()

    class Meta(BaseUserSerializer.Meta):
        fields = BaseUserSerializer.Meta.fields + ('password_confirmation', 'period')

    def validate_period(self, value):
        if value not in range(1,13):
            raise serializers.ValidationError(
                'Invalid period. Please choose one of the given periods'
            )

        return value

    def validate(self, data: dict) -> dict:
        """
            validate class method
            This method is used to make some validations and raise errors.
            In this particular case I'm validating if the passwords match.
        """
        password = data['password']
        password_confirmation = data['password_confirmation']

        if password != password_confirmation:
            raise serializers.ValidationError('Passwords don\'t match')

        password_validation.validate_password(password)

        return data

    def create(self, data: dict) -> dict:
        data.pop('password_confirmation')
        period = data.pop('period')
        user = User.objects.create_user(**data)
        return UserRoleFactory().create_user_role(user.user_role, user=user, period=period)


class TeacherSignUpSerializer(BaseUserSerializer):
    """
        User Sign Up Serializer.
        This serializer handles the process of validate data and create
        the user.
    """
    password_confirmation = serializers.CharField(
        min_length=8
    )

    academic_grade = serializers.IntegerField()

    number = serializers.CharField(
        max_length=10
    )

    office_number = serializers.CharField(
        max_length=10
    )

    class Meta(BaseUserSerializer.Meta):
        fields = BaseUserSerializer.Meta.fields + ('password_confirmation', 'academic_grade', 'number', 'office_number')

    def validate_period(self, value):
        if value not in range(1,13):
            raise serializers.ValidationError(
                'Invalid period. Please choose one of the given periods'
            )

        return value

    def validate(self, data: dict) -> dict:
        """
            validate class method
            This method is used to make some validations and raise errors.
            In this particular case I'm validating if the passwords match.
        """
        password = data['password']
        password_confirmation = data['password_confirmation']

        if password != password_confirmation:
            raise serializers.ValidationError('Passwords don\'t match')

        password_validation.validate_password(password)

        return data

    def create(self, data: dict) -> dict:
        data.pop('password_confirmation')
        academic_grade = data.pop('academic_grade')
        number = data.pop('number')
        office_number = data.pop('office_number')
        user = User.objects.create_user(**data)
        return UserRoleFactory().create_user_role(user.user_role, user=user, academic_grade=academic_grade, number=number, office_number=office_number)

