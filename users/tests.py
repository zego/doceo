# django
from django.test import TestCase, client

# models
from users.models import User
from teachers.models import Teacher

# Utilities
import json


# Create your tests here.
class AuthenticationTests(TestCase):
    base_url = '/v1/users'

    def setUp(self):
        user = {
            'email': 'miguelDz@cobra.com',
            'password': 'FmiYtGmoLLaZpLathDL',
            'password_confirmation': 'FmiYtGmoLLaZpLathDL',
            'dossier': 210161,
            'first_name': 'Miguel',
            'last_name': 'Diaz',
            'user_role': 1,
            'period': 8
        }
        response = self.client.post(
            f'{self.base_url}/signup/',
            data=user
        )

    def test_user_signup(self):
        user_data = {
            'email': 'jhon_doe@mail.com',
            'password': 'FmiYtGmoLLaZpLathDL',
            'password_confirmation': 'FmiYtGmoLLaZpLathDL',
            'dossier': 200536,
            'first_name': 'Jhon',
            'last_name': 'Doe',
            'user_role': 2,
            'academic_grade': 2,
            'number': '4422101616',
            'office_number': 'C10'
        }
        response = self.client.post(
            f'{self.base_url}/signup/',
            data=user_data
        )
        json_response = json.loads(response.content)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(json_response['message'], 'User created succesfully.')
        self.assertEqual(User.objects.all().count(), 2)
        self.assertEqual(Teacher.objects.all().count(), 1)

    def test_user_login(self):
        response = self.client.post(
            f'{self.base_url}/login/',
            data={'email': 'miguelDz@cobra.com', 'password': 'FmiYtGmoLLaZpLathDL'}
        )
        json_response = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(json_response.get('access-token'))
        self.assertIsNotNone(json_response.get('refresh-token'))
        self.assertEqual(len(response.cookies), 2)
        self.assertIsNotNone(response.cookies.get('csrftoken'))
        self.assertIsNotNone(response.cookies.get('key'))

    def test_user_logout(self):
        response_login = self.client.post(
            f'{self.base_url}/login/',
            data={'email': 'miguelDz@cobra.com', 'password': 'FmiYtGmoLLaZpLathDL'}
        )
        json_response_login = json.loads(response_login.content)
        token = json_response_login.get('access-token')
        response = self.client.post(
            f'{self.base_url}/logout/',
            HTTP_AUTHORIZATION=f'Bearer {token}'
        )
        json_response = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json_response['message'], 'User logged out succesfully')
        self.assertIsNone(response.cookies.get('access-token', None))
        self.assertIsNone(response.cookies.get('refresh-token', None))
