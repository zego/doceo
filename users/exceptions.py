class UserRoleException(Exception):

    def __init__(self, message=None):
        if message:
            self.message = message
        super().__init__(self.message)
