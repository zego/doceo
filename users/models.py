# Django
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager 

# Utilities
from users.utils import get_upload_path

# Choices
ROLES = (
    (1, 'Student'),
    (2, 'Teacher'),
    (3, 'Administrator'),
    (4, 'Super Administrator'),
)


# Create your models here.
class UserManager(BaseUserManager):
    """
        Manager for User model.
        Define a model manager for User model with no username field.
    """

    use_in_migrations: bool = True

    def _create_user(self, email: str, password: str, **extra_fields: dict) -> 'User':
        """
            Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email: str, password: str, **extra_fields: dict) -> 'User':
        """
            Create and save a regular User with the given email and password.
        """
        if not password:
            raise ValueError('You must enter a password')

        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email: str, password: str, **extra_fields: dict) -> 'User':
        """
            Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('user_role', 1)
        extra_fields.setdefault('dossier', 1)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """
        User Model
        This model uses the default User class django provides but we add some extra data.    
    """
    username: None = None

    email: str = models.EmailField(
        verbose_name='email',
        unique=True,
    ) # Making the email field required and unique.

    dossier: int = models.IntegerField(
        null=False
    )

    user_role: int = models.SmallIntegerField(
        null=True,
        choices=ROLES,
        default=1
    )

    picture: str = models.ImageField(
        upload_to=get_upload_path,
        blank=True,
        null=True
    )

    about: str = models.TextField(
        blank=True
    )

    USERNAME_FIELD: str = 'email' # Removing the username field  and telling Django that you are going to use the email field as the USERNAME_FIELD.

    REQUIRED_FIELDS: list = [] # Removing the email field from the REQUIRED_FIELDS settings (it is automatically included as USERNAME_FIELD).

    objects: object = UserManager()

    def __str__(self) -> str:
        return f'User: {self.get_full_name()} - Dossier: {self.dossier}'
