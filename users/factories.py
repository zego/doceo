# utilities
from abc import ABC, abstractmethod
from typing import Union

# models
from students.models import Student
from teachers.models import Teacher
from admins.models import Administrator
from super_admins.models import SuperAdministrator

# Exceptions
from users.exceptions import UserRoleException


class AbstractUserRoleFactory(ABC):

    @abstractmethod
    def create_user_role(self, user_role, **kwargs) -> Union['SuperAdministrator', 'Administrator', 'Teacher', 'Student']:
        pass


class UserRoleFactory(AbstractUserRoleFactory):

    def create_user_role(self, user_role, **kwargs) -> Union['SuperAdministrator', 'Administrator', 'Teacher', 'Student']:
        if user_role == 1:
            student = Student.objects.create(**kwargs)
            return student
        elif user_role == 2:
            teacher = Teacher.objects.create(**kwargs)
            return teacher
        elif user_role == 3:
            administrator = Administrator.objects.create(**kwargs)
            return administrator
        elif user_role == 4:
            super_admin = SuperAdministrator.objects.create(**kwargs)
            return super_admin

        raise UserRoleException('The given user role is not a valid user role')
