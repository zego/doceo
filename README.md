# Brainly

Brainly API repo. This repo contains the BackEnd for Brainly system.

---

## Getting Started

The Back-End requirements can be found on requirements.txt file. Before installing them, it's important that you already have a virtual environment running.

### Create one on Windows:

```
python -m venv <your_virtualenv_name>
```

Activate your environment:

```
<your_virtualenv_name>\Scripts\activate
```

Deactivate your environment:

```
deactivate
```

### Create one on Ubuntu or Mac:

```
python3 -m venv <your_virtualenv_name>
```

Activate your environment:

```
source <your_virtualenv_name>/bin/activate
```

Deactivate your environment:

```
deactivate
```

Then type the next command to install requirements:

```
pip install -r requirements.txt
```

For Ubuntu:

```
pip3 install -r requirements.txt
```

## Useful commands for django

To update the migrations local files when a model changes

```
    python manage.py makemigrations
```

To sync D.B. with the migrations local files

```
    python manage.py migrate
```

Run server

```
    python manage.py runserver
```

Check for issues on project

```
    python manage.py check
```

Run tests

```
    python3 ./manage.py test --settings=doceo_api.settings
```
