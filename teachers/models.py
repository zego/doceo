# django
from django.db import models
from django.conf import settings

# constants
ACADEMIC_GRADES = (
    (1, 'Licenciatura'),
    (2, 'Ingeniaría'),
    (3, 'Maestria'),
    (4, 'Doctorado'),
)


# Create your models here.
class Teacher(models.Model):
    user = models.OneToOneField(
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    academic_grade = models.SmallIntegerField(
        choices=ACADEMIC_GRADES,
        default=1
    )

    number = models.CharField(
        max_length=10,
        unique=True
    )

    office_number = models.CharField(
        max_length=10
    )

    is_active = models.BooleanField(
        default=True
    )